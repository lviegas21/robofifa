import requests
from bs4 import BeautifulSoup
from datetime import datetime
import time
def last_chat_id(token):
    try:
        url = "https://api.telegram.org/bot{}/getUpdates".format(token)
        response = requests.get(url)
        if response.status_code == 200:
            json_msg = response.json()
            print(json_msg)
            for json_result in reversed(json_msg['result']):
                message_keys = json_result['message'].keys()
                if ('new_chat_member' in message_keys) or ('group_chat_created' in message_keys):
                    return json_result['message']['chat']['id']
            print('Nenhum grupo encontrado')
        else:
            print('A resposta falhou, código de status: {}'.format(response.status_code))
    except Exception as e:
        print("Erro no getUpdates:", e)

# enviar mensagens utilizando o bot para um chat específico
def send_message(token, chat_id, message,  parse_mode = 'Markdown'):
    print("Id do chat:", chat_id)
    hora_n = []
    time_right = []
    time_left = []
    last_casa = []
    last_fora = []
    por_casa = []
    por_fora = []
    link_scrapping = []
    handicamp = []
    url_site = 'https://www.totalcorner.com/league/view/12995'

    response = requests.get(url_site)
    html = BeautifulSoup(response.text, 'html.parser')

    for jogos in html.select('tr'):
        hora = jogos.select_one('td')
        time_r = jogos.select_one('.text-right a')
        time_l = jogos.select_one('.text-left a')
        if time_r == None:
            print('')
        if time_l == None:
            print('')
        else:
            fuso = hora.text
            var = fuso[0: 5]
            var2 = fuso[6: 8]
            var3 = int(var2)
            var_new = ''
            if var3 >= 0 and var3 <= 9:
                var_new = '0' + str(var3)
            else:
                var_new = str(var3)
            var4 = fuso[8:]
            hora_n.append(f'{var} {var_new}{var4}')
            time_right.append(time_r.string)
            time_left.append(time_l.string)


    for jogos in html.select('td'):
        hand = jogos.select_one('.text-center')
        if hand != None:
            handicamp.append(hand.text)

    url2 = 'https://www.totalcorner.com/'
    new_scrapping = ''
    response = requests.get(url_site)
    html = BeautifulSoup(response.text, 'html.parser')

    for links in html.select('tr'):
        link_partida = links.select_one('.text-center a')
        if link_partida != None:
            url_final = f'{link_partida["href"]}'
            new_scrapping = url2 + url_final[0:30]
            link_scrapping.append(new_scrapping)

    for c in range(len(link_scrapping)):
        print(link_scrapping[c])
        response_n = requests.get(link_scrapping[c])
        html = BeautifulSoup(response_n.text, 'html.parser')

        c = 0
        cont = 0
        for jogos in html.select('.match-facts-history'):
            link_partida = jogos.select_one('p')

            if c == 0:
                last_c = link_partida.text[33: 35]
                if last_c == '':
                    last_casa.append(0)
                else:
                    last_casa.append(last_c)

            elif c == 1:
                last_f = link_partida.text[33: 35]
                if last_f == '':
                    last_casa.append(0)
                else:
                    last_fora.append(last_f)

            c += 1
            cont+=1
        c = 0

        cont_c = 0
        for dados in html.select('.row100'):
            d = dados.select_one('.text-left')
            if cont_c < 2:
                if d != None:
                    porc = d.text[16:18]
                    if cont_c == 0:
                        porc_c = porc
                        por_casa.append(porc_c)
                        cont_c += 1
                    elif cont_c == 1:
                        porc_f = porc
                        por_fora.append(porc_f)
                        cont_c += 1

        cont_c = 0

    if len(por_casa) < 40:
        por_casa.append(0)
    if len(por_fora) < 40:
        por_fora.append(0)
    if len(last_casa) < 40:
        last_casa.append(0)
    if len(last_fora) < 40:
        last_fora.append(0)

    print(len(por_casa))
    print(len(por_fora))
    print(len(last_casa))
    print(len(last_fora))
    print(por_casa)
    print(por_fora)
    print(last_casa)
    print(last_fora)

    for c in range(len(hora_n)):
        comp = handicamp[c].replace('\n', '')
        print(time_right[c], 'X', time_left[c])
        horario = hora_n[c][0: 13]
        data = horario[0: 5]
        hora_exata = horario[5: 8]
        verifica = int(hora_exata) - 3
        minutos = horario[8:]
        if int(verifica) < 0 or int(verifica) == 0:
            verifica = verifica + 3
            if int(verifica) <= 3 and int(verifica) >= 0:
                hora_madrugada = 24 + int(verifica)
                horario_certo = hora_madrugada - 3
                if horario_certo == 24:
                    data_n = f"00{minutos}"
                else:
                    data_n = f"{int(horario_certo)}{minutos}"

        elif int(verifica) >= 0 and int(verifica) <= 2:
            data_n = f"{int(verifica)}{minutos}"

        elif int(verifica) < 10 and int(verifica) >= 3:
            data_n = f"{int(verifica)}{minutos}"
        elif int(verifica) >= 10 and int(verifica) <= 23:
            data_n = f"{verifica}{minutos}"

        data_n = str(data_n)

        horajogo = datetime.strptime(data_n, '%H:%M').time()
        horario_atual = datetime.now().strftime('%H:%M')
        compa = str(horajogo)
        horas1 = compa[0: 2]
        minutos1 = compa[3: 5]
        compa2 = str(horario_atual)
        horas2 = compa2[0: 2]
        minutos2 = compa2[3:5]
        # hora do jogo
        valor1 = (int(horas1) * 60) + int(minutos1)
        # hora atual
        valor2 = ((int(horas2) * 60) + int(minutos2)) - 240
        negrito = '*'
        var_hora = f'{verifica}{minutos}'
        url3 = 'https://www.bet365.com/#/IP/AC/B1/C1/D13/E47578772/F2/'

        if int(last_casa[c]) >= 10 and int(last_fora[c]) >= 10:
            print('times tem mais de 10 jogos')
            if comp == '0.0, -0.5' or comp == '0.0,-0.5' or comp == '-0.5':
                if int(por_casa[c]) >= 50 and int(por_fora[c]) <= 30:
                    print(horario, horario_atual)
                    print('minuto inicio do jogo', valor1 - valor2)
                    if (valor1 - 3) >= valor2 and (valor1 - 6) <= valor2:
                        print('mensagem vai ser enviada')
                        try:
                            message = f'🚨 {negrito}ALERTA{negrito} | 🤖 {negrito}ROBÔ FIFÃO{negrito} \n' \
                                      f'\n ⚽ {negrito}Jogo:{negrito} {time_right[c]} x {time_left[c]}\n' \
                                      f'\n ⏰ {negrito}Horário do Jogo:{negrito} {var_hora}\n' \
                                      f'\n ✅ {negrito}Entrada:{negrito} Back {time_right[c]}\n' \
                                      f'\n ⭐ {negrito}Sugestão:{negrito} Entrada com 1% do capital\n' \
                                      f'\n {url3}'

                            data = {"chat_id": chat_id, "text": message, 'parse_mode': 'Markdown'}
                            url = "https://api.telegram.org/bot{}/sendMessage".format(token)
                            requests.post(url, data)
                        except Exception as e:
                            print("Erro no sendMessage:", e)

        if int(last_casa[c]) >= 10 and int(last_fora[c]) >= 10:
            print('times tem mais de 10 jogos')
            if comp == '0.0, +0.5' or comp == '0.0,+0.5' or comp == '+0.5':
                if int(por_fora[c]) >= 50 and int(por_casa[c]) <= 30:
                    print(horario, horario_atual)
                    print('minuto inicio do jogo', valor1 - valor2)
                    if (valor1 - 3) >= valor2 and (valor1 - 6) <= valor2:
                        print('mensagem enviada')
                        try:
                            message = f'🚨 {negrito}ALERTA{negrito} | 🤖 {negrito}ROBÔ FIFÃO{negrito} \n' \
                                      f'\n⚽ {negrito}Jogo: {negrito}{time_right[c]} x {time_left[c]}\n' \
                                      f'\n⏰ {negrito}Horário do Jogo:{negrito} {var_hora}\n' \
                                      f'\n✅ {negrito}Entrada:{negrito} Back {time_left[c]}\n' \
                                      f'\n⭐ {negrito}Sugestão:{negrito} Entrada com 1% do capital\n' \
                                      f'\n {url3}'
                            data = {"chat_id": chat_id, "text": message, 'parse_mode': 'Markdown'}
                            url = "https://api.telegram.org/bot{}/sendMessage".format(token)
                            requests.post(url, data)
                        except Exception as e:
                            print("Erro no sendMessage:", e)
        time.sleep(4)


token = '1639022820:AAGLR6gcycB_S_S73VZtMrOK0farhix0efo'

while True:
    # id do chat que será enviado as mensagens
    '-1001261537682'
    'last_chat_id(token)'
    chat_id = '-1001261537682'
    message = ''

    # enviar a mensagem
    send_message(token, chat_id, message)









# print(horario)

