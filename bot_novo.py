import requests
from bs4 import BeautifulSoup
from datetime import datetime
import time
def last_chat_id(token):
    try:
        url = "https://api.telegram.org/bot{}/getUpdates".format(token)
        response = requests.get(url)
        if response.status_code == 200:
            json_msg = response.json()
            print(json_msg)
            for json_result in reversed(json_msg['result']):
                message_keys = json_result['message'].keys()
                if ('new_chat_member' in message_keys) or ('group_chat_created' in message_keys):
                    return json_result['message']['chat']['id']
            print('Nenhum grupo encontrado')
        else:
            print('A resposta falhou, código de status: {}'.format(response.status_code))
    except Exception as e:
        print("Erro no getUpdates:", e)

# enviar mensagens utilizando o bot para um chat específico
def send_message(token, chat_id, message,  parse_mode = 'Markdown'):
    print("Id do chat:", chat_id)
    hora_n = []
    time_right = []
    time_left = []
    link_scrapping = []
    handicamp = []
    url_site = 'https://www.totalcorner.com/league/view/12995'

    response = requests.get(url_site)
    html = BeautifulSoup(response.text, 'html.parser')

    for jogos in html.select('tr'):
        hora = jogos.select_one('td')
        time_r = jogos.select_one('.text-right a')
        time_l = jogos.select_one('.text-left a')
        if time_r == None:
            print('')
        if time_l == None:
            print('')
        else:
            fuso = hora.text
            var = fuso[0: 5]
            var2 = fuso[6: 8]
            var3 = int(var2)
            var_new = ''
            if var3 >= 0 and var3 <= 9:
                var_new = '0' + str(var3)
            else:
                var_new = str(var3)
            var4 = fuso[8:]
            hora_n.append(f'{var} {var_new}{var4}')
            time_right.append(time_r.string)
            time_left.append(time_l.string)


    for jogos in html.select('td'):
        hand = jogos.select_one('.text-center')
        if hand != None:
            handicamp.append(hand.text)

    url2 = 'https://www.totalcorner.com/'
    new_scrapping = ''
    response = requests.get(url_site)
    html = BeautifulSoup(response.text, 'html.parser')

    for links in html.select('tr'):
        link_partida = links.select_one('.text-center a')
        if link_partida != None:
            url_final = f'{link_partida["href"]}'
            new_scrapping = url2 + url_final[0:30]
            link_scrapping.append(new_scrapping)
    print('len do scrapping: ', len(link_scrapping))
    cont = 0
    for c in range(len(link_scrapping)):
        print(link_scrapping[c])
        response_n = requests.get(link_scrapping[c])
        html = BeautifulSoup(response_n.text, 'html.parser')
        variavel = '0'
        score_c = '0'
        score_f = '0'
        dang_c = '0'
        dang_f = '0'
        for status in html.select('p'):
            var = status.select_one('.red')
            if var != None:
                variavel = var.text
                break
        value = variavel.replace(' ', '')
        c = 0
        for status in html.select('.panel-body p'):
            score = status.select_one('span')
            if score != None:
                if c == 0:

                    if 'full' in value:
                        score_c = status.text[23:26].replace(' ', '')
                        score_f = status.text[27:29].replace(' ', '')
                        break

                    elif len(value) > 0:
                        if len(value) < 3:
                            score_c = '0' + status.text[24: 25].replace('\n', '')
                            score_f = '0' + status.text[28: 29].replace('\n', '')
                            break
                        else:
                            score_c = status.text[24: 25].replace(' ', '')
                            score_f = status.text[27: 29].replace(' ', '')
                            break
                    else:
                        score_c = status.text[20:22].replace(' ', '')
                        score_f = status.text[24:25].replace(' ', '')
                        break

            c = c + 1
        placar_d = score_c
        atta = 0
        attack_l = 0
        for status in html.select('.row'):
            dangs_c = status.select_one('.text-right')
            dangs_f = status.select_one('.text-left')

            # jogo full
            if dangs_c != None and dangs_f != None:
                if len(value) > 3 and len(value) < 4:
                    if atta == 8 and attack_l == 8:
                        dang_c = dangs_c.text
                        dang_f = dangs_f.text
                        break
                else:
                    if atta == 8 and attack_l == 8:
                        if len(dangs_c.text) > 3 and len(dangs_f.text) > 3:
                            dang_c = 0
                            dang_f = 0
                            break
                        else:
                            dang_c = dangs_c.text
                            dang_f = dangs_f.text
                            break



                atta = atta + 1
                attack_l = attack_l + 1


        comp = handicamp[cont].replace('\n', '')
        print(time_right[cont], {score_c}, 'X', {score_f} , time_left[cont])
        print('attacks casa: ', dang_c, 'attacks fora: ', dang_f)
        print('score time da casa: ', score_c, 'score time de fora: ', score_f)
        horario = hora_n[cont][0: 13]
        data = horario[0: 5]
        hora_exata = horario[5: 8]
        verifica = int(hora_exata) - 3
        minutos = horario[8:]
        if int(verifica) < 0 or int(verifica) == 0:
            verifica = verifica + 3
            if int(verifica) <= 3 and int(verifica) >= 0:
                hora_madrugada = 24 + int(verifica)
                horario_certo = hora_madrugada - 3
                if horario_certo == 24:
                    data_n = f"00{minutos}"
                else:
                    data_n = f"{int(horario_certo)}{minutos}"

        elif int(verifica) >= 0 and int(verifica) <= 2:
            data_n = f"{int(verifica)}{minutos}"

        elif int(verifica) < 10 and int(verifica) >= 3:
            data_n = f"{int(verifica)}{minutos}"
        elif int(verifica) >= 10 and int(verifica) <= 23:
            data_n = f"{verifica}{minutos}"

        data_n = str(data_n)
        print('loop', cont)
        horajogo = datetime.strptime(data_n, '%H:%M').time()
        horario_atual = datetime.now().strftime('%H:%M')
        compa = str(horajogo)
        horas1 = compa[0: 2]
        minutos1 = compa[3: 5]
        compa2 = str(horario_atual)
        horas2 = compa2[0: 2]
        minutos2 = compa2[3:5]
        # hora do jogo
        valor1 = (int(horas1) * 60) + int(minutos1)
        # hora atual
        valor2 = ((int(horas2) * 60) + int(minutos2))
        negrito = '*'
        var_hora = f'{verifica}{minutos}'
        url3 = 'https://www.bet365.com/#/IP/AC/B1/C1/D13/E47578772/F2/'
        print(horario, horario_atual)
        valor3 = (valor1 - valor2) - 240
        if valor3 < -15:
            if not 'full' == value:
                valor3 = (valor1 - valor2) + 120
        valor4 = valor3
        print('minutos para o inicio: ', valor4)


        # if valor4 > 0:
        #     dang_c = dang_cs
        #     dang_f = dang_fo


        if valor4 <= -3 and valor4 >= -4:
            print('tempo passou')
            valor = ''
            valor2 = ''
            if (int(score_c) == int(score_f) and int(score_c) > 0 and int(score_f) > 0)\
            or int(score_c) > int(score_f) and int(score_c) == (int(score_f) + 1)\
            or int(score_f) > int(score_c) and int(score_f) == (int(score_c) + 1):
                print('times com o mesmo placar')
                if int(dang_c) >= 8 or int(dang_f) >= 8:
                    print(dang_c, dang_f)
                    print('mensagem vai ser enviada')
                    try:
                        print('oi')
                        message = f'🚨 {negrito}ALERTA{negrito} | 🤖 {negrito}ROBÔ FIFÃO{negrito} \n' \
                                  f'\n ⚽ {negrito}Jogo:{negrito} {time_right[cont]} {score_c} x {score_f} {time_left[cont]}\n'\
                                  f'\n ✅ {negrito}Entrada:{negrito} Mais de {int(score_c) + int(score_f) + 1} gol(s) (odds asiáticas)\n' \
                                  f'\n ⭐ {negrito}Sugestão:{negrito} Entrada com 1% do capital\n' \
                                  f'\n {url3}'

                        data = {"chat_id": chat_id, "text": message, 'parse_mode': 'Markdown'}
                        url = "https://api.telegram.org/bot{}/sendMessage".format(token)
                        requests.post(url, data)
                    except Exception as e:
                        print("Erro no sendMessage:", e)
        time.sleep(2)

        cont+=1
        if cont == len(link_scrapping):
            cont = 0


token = '1639022820:AAGLR6gcycB_S_S73VZtMrOK0farhix0efo'

while True:
    # id do chat que será enviado as mensagens
    '-1001261537682'
    'last_chat_id(token)'
    chat_id = '-1001261537682'
    message = ''

    # enviar a mensagem
    send_message(token, chat_id, message)
